import React from 'react';
import { View, TouchableNativeFeedback, Text, StyleSheet } from 'react-native';

const Button = props => (
  <TouchableNativeFeedback style={styles.container} onPress={props.onPress}>
    <View style={styles.container}>
      <Text style={styles.title}>{props.title}</Text>
    </View>
  </TouchableNativeFeedback>
);

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#9933FF',
    height: 60,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    color: '#FFFFFF',
    fontSize: 20,
    fontWeight: 'bold'
  }
});

export default Button;
