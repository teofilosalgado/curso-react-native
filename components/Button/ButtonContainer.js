import React from 'react';

import Button from './Button';

export default class ButtonContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <Button title={this.props.title} onPress={this.props.onPress} />;
  }
}
