import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

import Button from './../components/Button/ButtonContainer';

export default class DetailScreen extends React.Component {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={{ color: '#FFFFFF' }}>Tela 2!</Text>
        <Button
          title='Ir para a tela 1'
          onPress={() => {
            this.props.navigation.navigate('home');
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#000000'
  }
});
