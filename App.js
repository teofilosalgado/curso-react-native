import { createStackNavigator, createAppContainer } from 'react-navigation';

import HomeScreen from './screens/HomeScreen';
import DetailScreen from './screens/DetailScreen';

const Navigator = createStackNavigator({
  home: {
    screen: HomeScreen
  },
  detail: {
    screen: DetailScreen
  }
});

export default createAppContainer(Navigator);
